import 'package:flutter/material.dart';
import 'package:netflix_fake/src/widgets/CustomListTile.dart';

class HomePage extends StatelessWidget {
  
  final String title;
  const HomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CustomListTile(title: "Populares na Netflix"),
            CustomListTile(title: "Em alta"),
            CustomListTile(title: "Assista novamente"),
            CustomListTile(title: "Assistidos recentemente"),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.cast, color: Colors.white,), backgroundColor: Colors.grey[800], onPressed: null,
      ),
    );
  }
}
