import 'package:netflix_fake/src/home/view/GenresModule.dart';

class MovieModule extends GenersModule {
  final int id;
  final String title;
  final String overview;
  final String language;
  final bool adult;
  final String posterPath;

  MovieModule({
      this.id,
      this.title,
      this.overview,
      this.language,
      this.adult,
      this.posterPath,
      idGeners,
      nameGeners
  }) : super(idGeners: idGeners, nameGeners: nameGeners);

  void getFilmes() {
    
  }

}