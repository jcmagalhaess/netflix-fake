import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:netflix_fake/src/widgets/CustomImageButton.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final String category;

  const CustomListTile({Key key, this.title, this.category}) : super(key: key);

  _returnMovie() async {
    var dio = Dio();
    Response response = await dio.get(
        "https://api.themoviedb.org/3/configuration?api_key=29c6968a88fc1b5b67313473d9cbd007");

    return print(response.data['images']['base_url']);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Text(title,
                  style: TextStyle(
                    fontSize: 20,
                  )),
            )),
        Container(
          width: double.infinity,
          height: 150,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
              CustomImageButton(
                title: "Teste",
                urlImage: "https://i.pinimg.com/originals/5d/68/49/5d6849092aea2a932f2a7f201bea4a66.jpg",
              ),
            ],
          ),
        ),
      ],
    );
  }
}
