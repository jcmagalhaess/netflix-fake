import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:netflix_fake/src/home/view/MovieModule.dart';

class CustomImageButton extends StatelessWidget {
  final String title;
  final String urlImage;

  void getHttp() async {
    try {
      Response response = await Dio().get(
        "https://api.themoviedb.org/3/discover/movie?api_key=29c6968a88fc1b5b67313473d9cbd007&language=en-US&sort_by=original_title.asc&include_adult=false&include_video=false&page=1",
      );
      print(response);
    } catch (e) {
      print(e);
    }
  }

  const CustomImageButton({Key key, this.title, this.urlImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2.5),
      child: InkWell(
        child: RaisedButton(
          padding: EdgeInsets.all(0),
          onPressed: () { 
            MovieModule movie = MovieModule(title: 'Interstellar', nameGeners: 'Cientifico');
            return print("${movie.title} tem um genero de filme do tipo ${movie.nameGeners}");
          },
          child: Container(
            child: Image.network(urlImage, fit: BoxFit.fill,),
          ),
        ),
      ),
    );
  }
}
