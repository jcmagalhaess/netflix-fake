import 'package:flutter/material.dart';
import 'package:netflix_fake/src/home/view/HomePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Netflix Fake',
      theme: ThemeData.dark(),
      home: Scaffold(
        drawer: Drawer(),
        appBar: AppBar(
          title: Text("Netflix Fake"),  
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.cast),
                onPressed: null,
            ),
            IconButton(
                icon: Icon(Icons.search),
                onPressed: null,
            ),
          ],
        ),
        body: HomePage(title: "Netflix Fake",),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem> [
            BottomNavigationBarItem(icon: Icon(Icons.home, size: 20,), title: Text('Início', style: TextStyle(fontSize: 12),), ),
            BottomNavigationBarItem(icon: Icon(Icons.search, size: 20,), title: Text('Buscar', style: TextStyle(fontSize: 12),)),
            BottomNavigationBarItem(icon: Icon(Icons.live_tv, size: 20,), title: Text('Em breve', style: TextStyle(fontSize: 12),)),
            BottomNavigationBarItem(icon: Icon(Icons.file_download, size: 20,), title: Text('Downloads', style: TextStyle(fontSize: 12),)),
            BottomNavigationBarItem(icon: Icon(Icons.menu, size: 20,), title: Text('Mais', style: TextStyle(fontSize: 12),)),
          ]
        ),
      )
    );
  }
}